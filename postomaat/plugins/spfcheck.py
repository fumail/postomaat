# -*- coding: UTF-8 -*-
#   Copyright 2012-2020 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#

from postomaat.shared import ScannerPlugin, DUNNO, strip_address, extract_domain, apply_template, \
    FileList, string_to_actioncode, get_default_cache, Cache
from postomaat.extensions.sql import SQL_EXTENSION_ENABLED, get_session, get_domain_setting
from postomaat.extensions.dnsquery import lookup, QTYPE_MX, QTYPE_TXT, DNSQUERY_EXTENSION_ENABLED
import re
import os
import fnmatch
import ipaddress
try:
    import spf
    HAVE_SPF = True
except ImportError:
    spf = None
    HAVE_SPF = False



class SPFPlugin(ScannerPlugin):
    """This plugin performs SPF validation using the pyspf module https://pypi.python.org/pypi/pyspf/
    by default, it just logs the result (test mode)

    to enable actual rejection of messages, add a config option on_<resulttype> with a valid postfix action. eg:

    on_fail = REJECT

    valid result types are: 'pass', 'permerror', 'fail', 'temperror', 'softfail', 'none', and 'neutral'
    you probably want to define REJECT for fail and softfail
    
    
    operation mode examples
    -----------------------
    I want to reject all hard fails and accept all soft fails:
      - do not set domain_selective_spf_file
      - set selective_softfail to False
      - set on_fail to REJECT and on_softfail to DUNNO
      
    I want to reject all hard fails and all soft fails:
      - do not set domain selective_spf_file
      - set selective_softfail to False
      - set on_fail to REJECT and on_softfail to REJECT
      
    I only want to reject select hard and soft fails
      - set a domain_selective_spf_file and list the domains to be tested
      - set selective_softfail to False
      - set on_fail to REJECT and on_softfail to REJECT
      
    I want to reject all hard fails and only selected soft fails:
      - set a domain_selective_spf_file and list the domains to be tested for soft fail
      - set selective_softfail to True
      - set on_fail to REJECT and on_softfail to REJECT
      
    I want to reject select hard fails and accept all soft fails:
      - do not set domain selective_spf_file
      - set selective_softfail to True
      - set on_fail to REJECT and on_softfail to DUNNO
    
    """
                                         
    def __init__(self,config,section=None):
        ScannerPlugin.__init__(self,config,section)
        self.logger=self._logger()
        self.check_cache = Cache()
        
        self.requiredvars={
            'ip_whitelist_file':{
                'default':'',
                'description':'file containing a list of IP adresses or CIDR ranges to be exempted from SPF checks. 127.0.0.0/8 is always exempted',
            },
            'domain_whitelist_file':{
                'default':'',
                'description':'if this is non-empty, all except sender domains in this file will be checked for SPF. define exceptions by prefixing with ! (e.g. example.com !foo.example.com). define TLD wildcards using * (e.g. example.*)',
            },
            'domain_selective_spf_file':{
                'default':'',
                'description':'if this is non-empty, only sender domains in this file will be checked for SPF. define exceptions by prefixing with ! (e.g. example.com !foo.example.com). define TLD wildcards using * (e.g. example.*)',
            },
            'selective_softfail':{
                'default':'False',
                'description':'evaluate all senders for hard fails (unless listed in domain_whitelist_file) and only evaluate softfail for domains listed in domain_selective_spf_file',
            },
            'check_subdomain':{
                'default':'False',
                'description':'apply checks to subdomain of whitelisted/selective domains',
            },
            'dbconnection':{
                'default':'',
                'description':'SQLAlchemy Connection string, e.g. mysql://root@localhost/spfcheck?charset=utf8. Leave empty to disable SQL lookups',
            },
            'domain_sql_query':{
                'default':"SELECT check_spf from domain where domain_name=:domain",
                'description':'get from sql database :domain will be replaced with the actual domain name. must return field check_spf',
            },
            'on_fail':{
                'default':'DUNNO',
                'description':'Action for SPF fail.',
            },
            'on_softfail':{
                'default':'DUNNO',
                'description':'Action for SPF softfail.',
            },
            'messagetemplate':{
                'default':'SPF ${result} for domain ${from_domain} from ${client_address} : ${explanation}',
                'description':'reject message template for policy violators'
            },
            'max_lookups':{
                'default':'10',
                'description':'maximum number of lookups (RFC defaults to 10)',
            },
            'google_mx_exception':{
                'default':'True',
                'description':'always consider pass if mail is sent from google servers, MX points to google, and SPF record contains MX directive',
            },
        }
        
        self.private_nets = [
            ipaddress.ip_network('10.0.0.0/8'), # private network
            ipaddress.ip_network('127.0.0.0/8'), # localhost
            ipaddress.ip_network('169.254.0.0/16'), # link local
            ipaddress.ip_network('172.16.0.0/12'), # private network
            ipaddress.ip_network('192.168.0.0/16'), # private network
            ipaddress.ip_network('fe80::/10'), # ipv6 link local
            ipaddress.ip_network('::1/128'), # localhost
        ]
        
        self.ip_whitelist_loader=None
        self.ip_whitelist=[] # list of ipaddress.ip_network

        self.selective_domain_loader=None
        self.domain_whitelist_loader=None
        
    
    def __check_domain(self, domain, listentry, check_subdomain):
        listed = False
        if listentry == domain:
            listed = True
        elif check_subdomain and domain.endswith('.%s' % listentry):
            listed = True
        elif listentry.endswith('.*') and fnmatch.fnmatch(domain, listentry):
            listed = True
        elif check_subdomain and listentry.endswith('.*') and fnmatch.fnmatch(domain, '*.%s' % listentry):
            listed = True
        return listed
    
    
    def _domain_in_list(self, domain, domain_list, check_subdomain):
        listed = False
        
        # check if listed
        for item in domain_list:
            # skip exceptions
            if item.startswith('!'):
                continue
            listed = self.__check_domain(domain, item, check_subdomain)
            if listed:
                break
        
        # if previous loop said listed, check for exceptions
        if listed:
            for item in domain_list:
                if item.startswith('!'):
                    item = item[1:]
                    listed = not self.__check_domain(domain, item, check_subdomain)
                    if not listed:
                        break
                     
        return listed
    
    
    def check_this_domain(self, from_domain):
        do_check = self.check_cache.get_cache(from_domain)
        if do_check is not None:
            return do_check
        
        do_check = None
        check_subdomain = self.config.getboolean(self.section,'check_subdomain')
        
        domain_whitelist_file = self.config.get(self.section,'domain_whitelist_file').strip()
        if domain_whitelist_file != '' and os.path.exists(domain_whitelist_file):
            if self.domain_whitelist_loader is None:
                self.domain_whitelist_loader = FileList(domain_whitelist_file, lowercase=True)
            if not self._domain_in_list(from_domain, self.domain_whitelist_loader.get_list(), check_subdomain):
                do_check = False
        
        if do_check is None:
            selective_sender_domain_file = self.config.get(self.section,'domain_selective_spf_file').strip()
            if selective_sender_domain_file != '' and os.path.exists(selective_sender_domain_file):
                if self.selective_domain_loader is None:
                    self.selective_domain_loader = FileList(selective_sender_domain_file, lowercase=True)
                if self._domain_in_list(from_domain, self.selective_domain_loader.get_list(), check_subdomain):
                    do_check = True
        
        if do_check is None:
            dbconnection = self.config.get(self.section, 'dbconnection').strip()
            sqlquery = self.config.get(self.section, 'domain_sql_query')
            
            if dbconnection!='' and SQL_EXTENSION_ENABLED:
                cache = get_default_cache()
                if get_domain_setting(from_domain, dbconnection, sqlquery, cache, self.section, False, self.logger):
                    do_check = True
            
            elif dbconnection!='' and not SQL_EXTENSION_ENABLED:
                self.logger.error('dbconnection specified but sqlalchemy not available - skipping db lookup')
        
        if do_check is None:
            do_check = False
        
        self.check_cache.put_cache(from_domain, do_check)
        return do_check


    def is_private_address(self,addr):
        ipaddr = ipaddress.ip_address(addr)
        private = False
        for net in self.private_nets:
            if ipaddr in net:
                private = True
                break
        return private


    def ip_whitelisted(self,addr):
        if self.is_private_address(addr):
            return True

        #check ip whitelist
        try:
            ip_whitelist_file = self.config.get(self.section, 'ip_whitelist_file').strip()
        except Exception:
            ip_whitelist_file = ''

        if ip_whitelist_file != '' and os.path.exists(ip_whitelist_file):
            plainlist = []
            if self.ip_whitelist_loader is None:
                self.ip_whitelist_loader=FileList(ip_whitelist_file,lowercase=True)

            #if self.ip_whitelist_loader.file_changed():
            # FIX: remove previous condition as it prevent loading the whitelist file.
            #      get_list() does handle file reload on change anyway.
            plainlist=self.ip_whitelist_loader.get_list()
            self.ip_whitelist=[ipaddress.ip_network(x) for x in plainlist]
            checkaddr=ipaddress.ip_address(addr)
            for net in self.ip_whitelist:
                if checkaddr in net:
                    return True
        return False
    
    
    def _google_mx_exception(self, suspect):
        client_name = suspect.get_value('client_name')
        if client_name and client_name.endswith('.google.com'):
            sender=suspect.get_value('sender')
            domainname = extract_domain(sender)
            result = []
            try:
                google_mx = False
                request = lookup(domainname, QTYPE_MX)
                for rec in request:
                    mx = rec['data'].lower()
                    if mx.endswith('.google.com'):
                        google_mx = True
                        break
                    
                if google_mx:
                    request = lookup(domainname, QTYPE_TXT)
                    for rec in request:
                        txt = rec['data'].strip('"').lower()
                        if re.search('^v=spf', txt):
                            result.append(txt)
            except Exception as e:
                self.logger.info('%s failed to lookup record for domain %s: %s: %s' % (suspect.get_value('queue_id'), domainname, e.__class__.__name__, str(e)))
            
            if len(result)==1 and re.search('\s\+?mx\s', result[0]):
                return True
        return False
    
    
    def examine(self,suspect):
        if not HAVE_SPF:
            return DUNNO
        
        client_address=suspect.get_value('client_address')
        helo_name=suspect.get_value('helo_name')
        sender=suspect.get_value('sender')
        queue_id = suspect.get_value('queue_id') or 'NOQUEUE'
        if client_address is None or helo_name is None or sender is None:
            self.logger.error('%s missing client_address or helo or sender' % queue_id)
            return DUNNO
        
        if self.ip_whitelisted(client_address):
            self.logger.info("%s client %s is whitelisted - no SPF check" % (queue_id, client_address))
            return DUNNO
        
        sender_email = strip_address(sender)
        if sender_email=='' or sender_email is None:
            return DUNNO
        
        sender_domain = extract_domain(sender_email)
        if sender_domain is None:
            self.logger.error('%s no domain found in sender address %s' % (queue_id, sender_email))
            return DUNNO
        
        sender_domain = sender_domain.lower()
        check_domain = self.check_this_domain(sender_domain)
        selective_softfail = self.config.getboolean(self.section, 'selective_softfail')
        if not check_domain and not selective_softfail: #selective_softfail is False: check all and filter later
            self.logger.debug('%s skipping SPF check for %s' % (queue_id, sender_domain))
            return DUNNO
        
        spf.MAX_LOOKUP = self.config.getint(self.section, 'max_lookups')
        try:
            result, explanation = spf.check2(client_address, sender_email, helo_name)
        except Exception as e:
            self.logger.error('%s failed to check SPF for %s due to: %s' % (queue_id, sender_domain, str(e)))
            result = 'none'
            explanation = ''
        
        google_mx_exception = self.config.getboolean(self.section,'google_mx_exception')
        if google_mx_exception and result in ['fail', 'softfail'] and helo_name.endswith('.google.com'):
            if self._google_mx_exception(suspect):
                self.logger.debug('%s overriding %s for %s due to google mx exception' % (queue_id, result, sender_domain))
                result = 'pass'
                explanation = 'google mx permit'
            
        suspect.tags['spf'] = result
        if result != 'none':
            self.logger.info('%s SPF client=%s, sender=%s, h=%s result=%s : %s' %
                             (suspect.get_value('queue_id'), client_address, sender_email, helo_name, result, explanation))
        
        action = DUNNO
        message = apply_template(self.config.get(self.section, 'messagetemplate'), suspect, dict(result=result, explanation=explanation))
        if result == 'fail' and (check_domain or selective_softfail):
            # reject on hard fail if domain is listed in domain_selective_spf_file or selective_softfail is enabled
            action = string_to_actioncode(self.config.get(self.section, 'on_fail'))
        elif result == 'softfail' and check_domain:
            # reject on soft fail if domain is listed in domain_selective_spf_file
            action = string_to_actioncode(self.config.get(self.section, 'on_softfail'))
        elif result == 'softfail' and not check_domain:
            # only log soft fail if domain is not listed in domain_selective_spf_file
            self.logger.info('%s ignoring SPF check for %s evaluating to softfail'% (queue_id, sender_domain))
        elif result not in ['fail', 'softfail']:
            # custom action for none, neutral, pass, permerr
            if self.config.has_option(self.section, 'on_%s' % result):
                action = string_to_actioncode(self.config.get(self.section, 'on_%s' % result))
            
        return action, message
    
    
    def lint(self):
        lint_ok = True
        
        if not DNSQUERY_EXTENSION_ENABLED:
            print('ERROR: no dns module installed')
            lint_ok = False
        
        if not HAVE_SPF:
            print('ERROR: pyspf or pydns module not installed - this plugin will do nothing')
            lint_ok = False

        if not self.checkConfig():
            print('ERROR: Error checking config')
            lint_ok = False
            
        domain_whitelist_file = self.config.get(self.section,'domain_whitelist_file').strip()
        if domain_whitelist_file != '' and not os.path.exists(domain_whitelist_file):
            print("INFO: domain_whitelist_file %s does not exist" % domain_whitelist_file)
            lint_ok = False
            
        selective_sender_domain_file=self.config.get(self.section,'domain_selective_spf_file').strip()
        if selective_sender_domain_file != '' and not os.path.exists(selective_sender_domain_file):
            print("INFO: domain_selective_spf_file %s does not exist" % selective_sender_domain_file)
            lint_ok = False
            
        if domain_whitelist_file and selective_sender_domain_file:
            print('WARNING: domain_whitelist_file and domain_selective_spf_file specified - whitelist has precedence, will check all domains and ignore domain_selective_spf_file')
            
        ip_whitelist_file=self.config.get(self.section,'ip_whitelist_file').strip()
        if ip_whitelist_file != '' and not os.path.exists(ip_whitelist_file):
            print("INFO: ip_whitelist_file %s does not exist - IP whitelist is disabled" % ip_whitelist_file)
            lint_ok = False
        
        sqlquery = self.config.get(self.section, 'domain_sql_query')
        dbconnection = self.config.get(self.section, 'dbconnection').strip()
        if not SQL_EXTENSION_ENABLED and dbconnection != '':
            print('INFO: SQLAlchemy not available, cannot use SQL backend')
            lint_ok = False
        elif dbconnection == '':
            print('INFO: No DB connection defined. Disabling SQL backend')
        else:
            if not sqlquery.lower().startswith('select '):
                lint_ok = False
                print('ERROR: SQL statement must be a SELECT query, got %s instead' % sqlquery.split()[0])
            if lint_ok:
                try:
                    conn=get_session(dbconnection)
                    conn.execute(sqlquery, {'domain':'example.com'})
                except Exception as e:
                    lint_ok = False
                    print(str(e))
        
        return lint_ok
    
    
    def __str__(self):
        return "SPF"
