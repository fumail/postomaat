# -*- coding: UTF-8 -*-
import logging
import re
import os
import typing as tp
import ipaddress as ia
try:
    _ = tp.OrderedDict
except AttributeError:
    # python 3.6 doesn't have a type for the OrderedDict, use normal Dict for typing
    tp.OrderedDict = tp.Dict

import yaml
from collections import OrderedDict

from postomaat.shared import ScannerPlugin, DUNNO, string_to_actioncode, apply_template, Suspect, ALLCODES
from .strategies import AVAILABLE_STRATEGIES, AVAILABLE_BACKENDS
from .strategies.backendint import BackendInterface


class CounterInt:
    """Interface for a counter"""
    def __init__(self, fieldname: str, setupdict: tp.Dict):
        self.fieldname = fieldname
        self.setupdict = setupdict
        self.regex = CounterInt._parse_regex(setupdict.get('regex', []))
        self.ipmatch = CounterInt._parse_ipmatch(setupdict.get('ipmatch', []))
        self.key = setupdict.get("key")

    def get_key(self, suspect: Suspect):
        """Get the key for this counter (equal to field entry unless specifically defined)"""
        return self.key if self.key else self.get_field(suspect=suspect)

    def get_field(self, suspect: Suspect):
        """Get field for counter, has to be implemented for each subclass"""
        raise NotImplementedError()

    def examine(self, suspect: Suspect) -> bool:
        """Examine field, return True on hit"""
        field = self.get_field(suspect=suspect)

        # convert a list to a single string
        if isinstance(field, (list, tuple)):
            field = " ".join(field)

        if field is None:
            # field not found/given/set skip this counter
            return False

        if self.regex:
            # If there is one (or several) regex defined,
            # only count on match
            for r in self.regex:
                if r.match(field):
                    return True
            return False
        elif self.ipmatch:
            # If there is one (or several) network ranges defined,
            # only count if ip is in net

            # create ip
            ip = ia.ip_address(field)

            # check if ip is in net
            for net in self.ipmatch:
                if ip in net:
                    return True
            return False
        else:
            # default is True, always count
            return True

    @staticmethod
    def _parse_regex(regex: tp.Union[str, list]) -> tp.List:
        """Compile regex string (or list of regex strings)"""
        assert isinstance(regex, (str, list))
        rlist = []

        inlist = [regex] if isinstance(regex, str) else regex
        for reg in inlist:
            try:
                compiled_regex = re.compile(reg)
                rlist.append(compiled_regex)
            except Exception as e:
                raise Exception(f"Orig regex: \"{reg}\" Erro: {e}").with_traceback(e.__traceback__)
        return rlist

    @staticmethod
    def _parse_ipmatch(netstring: tp.Union[str, list]) -> tp.List:
        """Parse ip network string"""
        assert isinstance(netstring, (str, list))
        nlist = []

        inlist = [netstring] if isinstance(netstring, str) else netstring
        for net in inlist:
            try:
                netobj = ia.ip_network(net, strict=False)
                nlist.append(netobj)
            except Exception as e:
                raise Exception(f"Orig network def: \"{net}\" Erro: {e}").with_traceback(e.__traceback__)
        return nlist


class CounterSuspectAttr(CounterInt):
    """Use an attribute of the Suspect"""
    def get_field(self, suspect: Suspect):
        try:
            return getattr(suspect, self.fieldname)
        except AttributeError:
            return None


class CounterSuspectTag(CounterInt):
    """Use a tag of the Suspect"""
    def get_field(self, suspect: Suspect):
        return suspect.get_tag(self.fieldname, None)


class CounterProtocolDict(CounterInt):
    """Use a field value of the Suspect dict, filled by protocol values"""
    def get_field(self, suspect: Suspect):
        return suspect.values.get(self.fieldname, None)


class Limiter:
    """Rule for RateLimiter"""
    CTYPE = {'suspect': CounterSuspectAttr,
             'protocoldict': CounterProtocolDict,
             'suspecttag': CounterSuspectTag
             }

    """Rule for ratelimit"""
    def __init__(self, name: str, setupdict: tp.Dict):
        # set priority, default is 0
        self.priority = setupdict.get('priority', None)
        self.priority = 0 if self.priority is None else int(self.priority)

        self.name = name
        self.number = None
        self.frame = None
        self.action = setupdict.get('action', "DUNNO").lower()
        assert self.action in ALLCODES, f"\"{self.action}\" not in {ALLCODES}"

        myclass = self.__class__.__name__
        loggername = "%s.limiter.%s" % (__package__, myclass)
        self.logger = logging.getLogger(loggername)

        self.counters = OrderedDict()
        self.strategy = setupdict.get('strategy', 'fixed')
        assert self.strategy in AVAILABLE_STRATEGIES, \
            f"{name}: \"{self.strategy}\" is not in available strategies \"{AVAILABLE_STRATEGIES}\""


        # extract rate
        self.number, self.frame = Limiter._parse_rate(setupdict.get('rate', ""))

        # reject message
        self.message = setupdict.get("message", "RateLimited")

        # add counters
        counters = setupdict.get('count', {})
        for cname, cdict in counters.items():
            self.counters[cname] = self.add_counter(countername=cname, counterdict=cdict)

        # setup sum method, default is 'event'
        self._sum_impl = {
            'data': Limiter._sum_data,
            'recipients': Limiter._sum_recipients,
            'event': Limiter._sum_event,
            'zero': Limiter._sum_zero,
        }.get(setupdict.get('sum', 'event'), Limiter._sum_event)

    def _sum_event(self, suspect: Suspect) -> int:
        """Sum events, called by 'sum'"""
        return 1

    def _sum_zero(self, suspect: Suspect) -> int:
        """Sum 0 (just check if already over), called by 'sum'"""
        return 0

    def _sum_recipients(self, suspect: Suspect) -> int:
        """Sum recipients (useful if called end-of-data), called by 'sum'"""
        numrecipients = suspect.values.get('recipient_count')
        if isinstance(numrecipients, str):
            numrecipients= int(numrecipients)
        if not isinstance(numrecipients, int):
            raise ValueError("Size is not integer, do you run postomaat end-of-data?")
        return numrecipients

    def _sum_data(self, suspect: Suspect) -> int:
        """Sum message data, called by 'sum'"""
        size = suspect.values.get('size')
        if isinstance(size, str):
            size = int(size)
        if not isinstance(size, int):
            raise ValueError("Size is not integer, do you run postomaat end-of-data?")
        numrecipients = suspect.values.get('recipient_count')
        if not numrecipients:
            numrecipients = 1
        else:
            numrecipients = int(numrecipients)
        return numrecipients*size

    def sum(self, suspect: Suspect) -> int:
        """Select correct method to sum"""
        return self._sum_impl(self, suspect=suspect)

    @staticmethod
    def _parse_rate(rate: tp.Union[tp.Dict[str, str], str]) -> tp.Tuple[tp.Optional[float], tp.Optional[float]]:
        """parse rate (number of messages in timeframe) which can be given as string or dict"""
        number, frame = None, None
        if isinstance(rate, str):
            if rate.count('/') != 1:
                raise ValueError("Define rate as x/y or use dict 'number: x, frame: y'")
            number, frame = [float(a) for a in rate.split('/')]
        elif isinstance(rate, dict):
            number = float(rate.get("number", None))
            number = float(number) if number else None
            frame = float(rate.get("frame", None))
            frame = float(frame) if frame else None
        return number, frame

    def add_counter(self, countername: str, counterdict: tp.Dict) -> CounterInt:
        """Extract type of counter, create object and store it in counters"""
        ctype = counterdict.get("type", '<not given>').lower()

        try:
            return Limiter.CTYPE[ctype](countername, counterdict)
        except AttributeError:
            raise ValueError(f"type \"{ctype}\" not a valid type for counter \"{countername}\"")

    def examine(self, suspect: Suspect):
        for cname, counter in self.counters.items():
            if counter.examine(suspect=suspect):
                self.logger.debug(f"{suspect.id} limiter match")
                return True
        return False

    def get_fieldvaluesdict(self, suspect: Suspect, use_keys: bool = False) -> OrderedDict:
        """Create a dict with fieldname,fieldnamevalue for all limiters """
        valdict = OrderedDict()
        for fieldname, count in self.counters.items():
            # return key values for f
            if use_keys:
                # get the key identifier for counter (equal to
                # field if not specifically defined)
                fieldvalue = count.get_key(suspect=suspect)
            else:
                fieldvalue = count.get_field(suspect=suspect)
            if fieldvalue is not None:
                valdict[fieldname] = fieldvalue
        return valdict

    def rejectmessage(self, suspect: Suspect):
        """Create rejectmessage, apply template"""
        return apply_template(self.message, suspect, values=self.get_fieldvaluesdict(suspect=suspect))


class RateLimitPlugin(ScannerPlugin):
    def __init__(self, config, section=None):
        ScannerPlugin.__init__(self, config, section)
        self.requiredvars = {

            'limiterfile': {
                'default': '/etc/postomaat/ratelimit.yml',
                'description': 'file based rate limits',
            },

            'backendtype': {
                'default': 'memory',
                'description': 'type of backend where the events are stored. memory is only recommended for low traffic standalone systems. alternatives are: redis, sqlalchemy' #pylint: disable=C0301
            },

            'backendconfig': {
                'default': '',
                'description': 'backend specific configuration. sqlalchemy: the database url, redis: redis://:[password]@hostname:6379/0' #pylint: disable=C0301
            }

        }

        self.logger = self._logger()
        self.logger.info("RateLimit plugin available backends: %s",
                         ' '.join([str(k) + " => " + str(AVAILABLE_BACKENDS[k].keys()) for k in AVAILABLE_BACKENDS.keys()]))

        # load limiters
        self.backends = {}
        self.logger.debug(f"Loading limiters")
        self.limiters = self.load_limiters()
        self.logger.debug(f"Loaded {len(self.limiters)} limiters")

        # after limiters, load backends for given backend and limiter strategies
        self.logger.debug(f"Loading backends")
        self.backends = self.load_backends()
        self.logger.debug(f"Init done")

    @staticmethod
    def yamlfile2dict(filename: str) -> tp.Dict:
        """Read yml file, return dict"""
        if not os.path.exists(filename):
            raise OSError(f"File {filename} does not exist!")
        with open(filename) as f:
            try:
                rawdict = yaml.full_load(f)
            except AttributeError:
                rawdict = yaml.load(f)
        return rawdict

    def load_limiters(self, catch_exceptions: bool = True) -> tp.OrderedDict[str, Limiter]:
        # load file to dict, setup Limiters
        limiterdict = OrderedDict()
        try:
            rawdict = RateLimitPlugin.yamlfile2dict(self.config.get(self.section, 'limiterfile'))
            self.logger.info(f"Loaded {len(rawdict)} limiters in dict")
            # now build dict
            odict = OrderedDict()
            prios = set()
            for lname, ldict in rawdict.items():
                self.logger.debug(f"Create limiter for \"{lname}\"")
                newlimiter = Limiter(name=lname, setupdict=ldict)
                odict[lname] = newlimiter
                prios.add(newlimiter.priority)
            if len(prios) > 1:
                self.logger.info("Different prios found -> sorting")
                limiterdict = OrderedDict(sorted(odict.items(), key=lambda kv: kv[1].priority))
            else:
                self.logger.info("No prios -> no sorting")
                limiterdict = OrderedDict(odict)
        except Exception as e:
            self.logger.error(str(e))
            if not catch_exceptions:
                raise Exception(str(e)).with_traceback(e.__traceback__)
        return limiterdict

    def examine(self, suspect: Suspect):
        """Main routine examining suspect, returncode from config if one limiter is exceeded"""
        self.logger.debug(f"{suspect.id} -> limites to check {list(self.limiters.keys())}")
        for lname, limiter in self.limiters.items():
            self.logger.debug(f"{suspect.id} -> check limiter \"{lname}\n")
            if limiter.number is None or limiter.number <= 0:
                self.logger.debug(f"Limiter {lname} is disabled (number={limiter.number})")
                continue
            if not limiter.examine(suspect=suspect):
                # limiter does not apply to this suspect
                self.logger.debug(f"Limiter {lname} does NOT apply to this suspect")
                continue
            else:
                # limiter applies to this suspect
                self.logger.debug(f"Limiter {lname} applies to this suspect")

            fieldvalues = list(limiter.get_fieldvaluesdict(suspect=suspect, use_keys=True).values())

            checkval = ','.join(fieldvalues)

            eventname = lname + checkval
            timespan = limiter.frame

            try:
                increment = limiter.sum(suspect=suspect)
                (allow, count) = self.backends[limiter.strategy].check_allowed(eventname, limiter.number, timespan, increment)
                self.logger.debug(f'Limiter event {eventname} (allow={allow}) count: {count}')
                if not allow:
                    return limiter.action, apply_template(limiter.message, suspect)
            except Exception as ex:
                error = type(ex).__name__, str(ex)
                self.logger.error(f'Failed to run limiter backend for strategy '
                                  f'"{limiter.strategy}" eventname {eventname} error {error}'
                                  )
                self.logger.debug("error traceback...", exc_info=ex)

        return DUNNO

    @property
    def required_strategies(self) -> tp.List:
        # return list with strategies for limiters registered
        return self.get_strategy_list()

    def get_strategy_list(self, limiters: tp.Optional[tp.Dict[str, Limiter]] = None) -> tp.List[str]:
        # if dict with limiters is given use this limiter, otherwise use limiters registered
        llist = limiters if limiters is not None else self.limiters
        blist = [l.strategy for l in llist.values()]
        return list(set(blist))

    def load_backends(self,
                      inlimitersdict: tp.Optional[tp.Dict[str, Limiter]] = None,
                      catch_exceptions: bool = True
                      ) -> tp.Dict[str, BackendInterface]:
        """ Of all the AVAILABLE_BACKENDS
        load only the backends required by limiters
        """
        if inlimitersdict:
            limitersdict = inlimitersdict
            required_strategies = self.get_strategy_list(limiters=limitersdict)
        else:
            limitersdict = self.limiters
            required_strategies = self.required_strategies

        backends = {}
        self.logger.debug(f"Loading backend for strategies: {required_strategies}")
        for strategy in required_strategies:
            self.logger.debug(f"Loading strategy \"{strategy}\"")
            btype = self.config.get(self.section, 'backendtype')
            if strategy in self.backends:
                # next loop for next strategy
                continue

            if btype not in AVAILABLE_BACKENDS[strategy]:
                errormsg = f'RateLimit backend {btype} not available for strategy {strategy}'
                self.logger.error(errormsg)
                if not catch_exceptions:
                    raise ValueError(errormsg)
                else:
                    continue
            backendconfig = self.config.get(self.section, 'backendconfig')

            try:
                backend_instance = AVAILABLE_BACKENDS[strategy][btype](backendconfig)
                backends.update(
                    {
                        strategy: backend_instance
                    }
                )
            except Exception as ex:
                error = type(ex).__name__, str(ex)
                errormsg = f'Failed to load backend {strategy}.{btype} error {error}'
                self.logger.error(errormsg)
                if not catch_exceptions:
                    raise Exception(errormsg).with_traceback(ex.__traceback__)
        return backends

    def lint(self) -> bool:
        from postomaat.funkyconsole import FunkyConsole
        fc = FunkyConsole()
        check_config = super().lint()
        if not check_config:
            print(fc.strcolor("ERROR - config check", "red"))
            return False
        try:
            limiters = self.load_limiters(catch_exceptions=False)
            print(f"Loading ({len(limiters)}) limiters: ok")
        except Exception as e:
            print(f"Error loading limiters: {str(e)}")
            print(fc.strcolor("ERROR - loading limiters", "red"))
            return False

        try:
            required_strategies = self.get_strategy_list(limiters=limiters)
            print(f"Strategies ({len(required_strategies)}): {required_strategies}")
        except Exception as e:
            print(f"Error loading strategies: {str(e)}")
            print(fc.strcolor("ERROR - loading strategies", "red"))
            return False

        try:
            backends = self.load_backends(inlimitersdict=limiters, catch_exceptions=False)
            print("Loading backends: ok")
        except Exception as e:
            print(f"Error loading backends: {str(e)}")
            print(fc.strcolor("ERROR - loading backends", "red"))
            return False
        return True
