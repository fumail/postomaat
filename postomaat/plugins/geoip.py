# -*- coding: UTF-8 -*-
#   Copyright 2012-2020 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#

"""
This plugin allows to reject mails based on the location of the sending server.

Set a blocklist to reject mail from specific countries.
Set a welcomelist to accept mail from specific countries only, mail from all other countries will be rejected.

The python pygeoip module and the GeoIP-database from MaxMind are required. 
"""

import os
from postomaat.shared import ScannerPlugin, DUNNO, REJECT, apply_template, FileList

LIB_GEOIP_NONE = 0
LIB_GEOIP_PYGEOIP = 1 # unknown origin/maintenance state
LIB_GEOIP_MAXMIND = 2 # deprecated
LIB_GEOIP_GEOIP2 = 3 # https://dev.maxmind.com/geoip/geoip2/geolite2/

try:
    import geoip2.database
    import geoip2.errors
    HAVE_GEOIP = LIB_GEOIP_GEOIP2
except ImportError:
    geoip2 = None
    try:
        import pygeoip
        HAVE_GEOIP = LIB_GEOIP_PYGEOIP
    except ImportError:
        pygeoip = None
        try:
            import GeoIP
            HAVE_GEOIP = LIB_GEOIP_MAXMIND
        except ImportError:
            GeoIP = None
            HAVE_GEOIP = LIB_GEOIP_NONE


class PyGeoIPCache(FileList):
    def __init__(self, filename, **kw):
        FileList.__init__(self, filename, **kw)
        self.geoip = None
        
        
    def _reload(self):
        self.geoip = pygeoip.GeoIP(self.filename)
        self.logger.debug('loaded geoip database %s' % self.filename)
        
    
    def country_code(self, ip):
        self._reload_if_necessary()
        try:
            cc = self.geoip.country_code_by_addr(ip)
        except Exception as e:
            self.logger.debug('Failed to get country code for %s: %s' % (ip, str(e)))
            cc = None
        return cc



class GeoIPCache(PyGeoIPCache):        
    def _reload(self):
        self.geoip = GeoIP.open(self.filename, GeoIP.GEOIP_STANDARD)
        self.logger.debug('loaded geoip database %s' % self.filename)
        
        
        
class Geoip2Cache(PyGeoIPCache):
    def _reload(self):
        self.geoip = geoip2.database.Reader(self.filename)
        self.logger.debug('loaded geoip database %s' % self.filename)
        
        
    def country_code(self, ip):
        try:
            data = self.geoip.country(ip)
            cc = data.country.iso_code
        except geoip2.errors.AddressNotFoundError:
            cc = None
        return cc



class GeoIPPlugin(ScannerPlugin):
                                         
    def __init__(self,config,section=None):
        ScannerPlugin.__init__(self,config,section)
        self.logger=self._logger()
        if HAVE_GEOIP == LIB_GEOIP_PYGEOIP:
            self.geoip = PyGeoIPCache(None)
        elif HAVE_GEOIP == LIB_GEOIP_MAXMIND:
            self.geoip = GeoIPCache(None)
        elif HAVE_GEOIP == LIB_GEOIP_GEOIP2:
            self.geoip = Geoip2Cache(None)
        else:
            self.geoip = None
        
        self.requiredvars={
            'database':{
                'default':'/var/lib/geoip/GeoLite2-Country.mmdb',
                'description':'location of the MaxMind GeopIP database file',
            },
            'blocklist':{
                'default':'',
                'description':'list of countries you do not want to receive mail from.',
            },
            'welcomelist':{
                'default':'',
                'description':'list of countries you want want to receive mail from. all other countries will be rejected. If you specify a welcomelist, the blocklist will have no function.',
            },
            'on_unknown':{
                'default':'DUNNO',
                'description':'what to do with unknown countries? this affects local IP-addresses. Set this to DUNNO or REJECT',
            },
            'reject_message':{
                'default':'this system does not accept mail from servers in your country "${cc}" - request welcomelisting',
                'description':'message displayed to client on reject. use ${cc} as placeholder for country code',
            },
            'reject_message_unknown': {
                'default': 'this system does not accept mail from your server - request welcomelisting',
                'description': 'message displayed to client on reject because cc is unknown.',
            },
        }



    def _get_list(self, list_type='blocklist'):
        data = self.config.get(self.section, list_type).strip()
        mylist = []
        if data:
            sep = ' '
            if ',' in data:
                sep = ','
            mylist = [i.strip() for i in data.split(sep)]
        return mylist
        
        
        
    def examine(self,suspect):
        if HAVE_GEOIP == LIB_GEOIP_NONE:
            return DUNNO
        
        database = self.config.get(self.section, 'database')
        if not os.path.exists(database):
            return DUNNO
        self.geoip.filename = database
        
        client_address=suspect.get_value('client_address')
        if client_address is None:
            self.logger.info('%s No client address found' % suspect.get_value('queue_id'))
            return DUNNO
        
        blocklist = self._get_list('blocklist')
        welcomelist = self._get_list('welcomelist')
        on_unknown = self.config.get(self.section, 'on_unknown')
        unknown_action = DUNNO
        if on_unknown.strip().upper() == 'REJECT':
            unknown_action = REJECT
        message = None
        
        cc = self.geoip.country_code(client_address)
        if cc is None:
            action = unknown_action
            if action == REJECT:
                rejmsg = self.config.get(self.section, 'reject_message_unknown').strip()
                message = apply_template(rejmsg, suspect, dict(cc=cc))
        
        else:
            action = DUNNO
            
            if cc in blocklist or (welcomelist and cc not in welcomelist):
                action = REJECT
                
            if action == REJECT:
                rejmsg = self.config.get(self.section, 'reject_message').strip()
                message = apply_template(rejmsg, suspect, dict(cn=cc, cc=cc)) # cn is deprecated as no internal support in geoip2

        self.logger.debug('%s IP: %s country: %s action: %s' % (suspect.get_value('queue_id'), client_address, cc, action))
        return action, message
        
        
    
    def lint(self):
        lint_ok = True
        
        if HAVE_GEOIP == LIB_GEOIP_NONE:
            print('No geoip module installed - this plugin will do nothing')
            lint_ok = False
        elif HAVE_GEOIP == LIB_GEOIP_PYGEOIP:
            print('using pygeoip')
        elif HAVE_GEOIP == LIB_GEOIP_MAXMIND:
            print('WARNING: using deprecated maxmind geoip')
        elif HAVE_GEOIP == LIB_GEOIP_GEOIP2:
            print('using geoip2')
        
            
        database = self.config.get(self.section, 'database')
        if not os.path.exists(database):
            print('Could not find geoip database file - this plugin will do nothing')
            lint_ok = False
        else:
            print('Using GeoIP Database in %s' % database)
        
        if lint_ok:
            testip = '8.8.8.8'
            self.geoip.filename = database
            cc = self.geoip.country_code(testip)
            print('Test IP %s is located in %s' % (testip, cc))
        
        if not self.checkConfig():
            print('Error checking config')
            lint_ok = False

        blocklist = self._get_list('blocklist')
        welcomelist = self._get_list('welcomelist')
        if not blocklist and not welcomelist:
            print('Neither block nor welcome list defined')
            lint_ok = False
        elif blocklist and welcomelist:
            print('Block and white list defined - only using blocklist')
            lint_ok = False
        else:
            print('Blocklist: %s' % blocklist)
            print('Welcomelist: %s' % welcomelist)

        return lint_ok
        
        

    def __str__(self):
        return "GeoIPPlugin"
