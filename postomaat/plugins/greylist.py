# -*- coding: UTF-8 -*-
#   Copyright 2012-2020 Fumail Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#

"""
Greylist implementation
EXPERIMENTAL - this plugin has not yet been tested and
is currently not being used in production. YMMV!
"""


import socket
import logging
import time
import os
import ipaddress
from postomaat.shared import ScannerPlugin, DUNNO, DEFER, FileList, apply_template
from postomaat.extensions.redisext import RedisKeepAlive, redis, ENABLED as REDIS_ENABLED
from domainmagic.tld import TLDMagic
from domainmagic.validators import is_ip, is_cidrv4



class GreyList(ScannerPlugin):
    
    def __init__(self,config,section=None):
        ScannerPlugin.__init__(self,config,section)
        self.logger=self._logger()
        self.requiredvars={
            'use_from_domain':{
                'default':'False',
                'description':'set to True to use only domain part of sender, else use full address',
            },
            'use_sender_host':{
                'default':'True',
                'description':'set to True to use sender host',
            },
            'sender_cidr_size':{
                'default':'24',
                'description':'if sender server has no PTR use CIDR of given size',
            },
            'use_to_domain': {
                'default': 'False',
                'description': 'set to True to use only domain part of recipient, else use full address',
            },
            'redis': {
                'default': 'localhost:6379:0',
                'description': 'redis config: host:port:db',
            },
            'redis_password': {
                'default': '',
                'description': 'password to connect to redis database. leave empty for no password',
            },
            'redis_timeout': {
                'default': '2',
                'description': 'timeout in seconds'
            },
            'greylist_ttl': {
                'default': '604800',
                'description': 'greylist ttl',
            },
            'greylist_minwait': {
                'default': '300',
                'description': 'greylist minimal wait after first sending attempt',
            },
            'messagetemplate':{
                'default':'greylisted - please come back in a few minutes',
                'description':'reject message template for blocked senders'
            },
            'skiplist_sender': {
                'default': '',
                'description': 'path to file with skipped senders',
            },
            'skiplist_hostnames': {
                'default': '',
                'description': 'path to file with skipped sender host names',
            },
            'skiplist_hostips': {
                'default': '',
                'description': 'path to file with skipped sender host ips or cidrs',
            },
            'check_spf_result': {
                'default': 'none, permerror',
                'description': 'only check if spf result returned one of these states (comma separated list)',
            },
        }
        self.tldmagic = TLDMagic()
        self.backend = None
        self.skiplist_sender = None
        self.skiplist_hostnames = None
        self.skiplist_hostips = None
        
        
        
    def _init_backend(self):
        if self.backend is not None:
            return
        host, port, db = self.config.get(self.section, 'redis').split(':')
        password = self.config.get(self.section, 'redis_password') or None
        red = RedisKeepAlive(
            host=host,
            port=port,
            db=int(db),
            password=password,
            socket_keepalive=True,
            socket_timeout=self.config.getint(self.section, 'redis_timeout'))
        self.backend = RedisBackend(red)
        self.backend.ttl = self.config.getint(self.section, 'ttl')
    
    
    
    def _normalise_cidr(self, ip):
        netmask = self.config.getint(self.config, 'sender_cidr_size')
        net = ipaddress.ip_network('%s/%s' % (ip, netmask), False)
        return '%s/%s' % (net.network, net.prefixlen)
    
    
    
    def _skip_tags(self, suspect):
        queue_id = suspect.get_value('queue_id') or 'NOQUEUE'
        spfresult = suspect.get_tag('spf')
        spfcheck = [s.strip for s in self.config.get(self.section, 'check_spf_result').split(',')]
        if spfcheck and spfresult in spfcheck:
            return False
        else:
            self.logger.debug('%s greylist: skipping due to SPF result %s' % (queue_id, spfresult))
        return True
    
    
    
    def _skip_sender(self, suspect):
        if self.skiplist_sender is None:
            filepath = self.config.get(self.section, 'skiplist_sender')
            if filepath and os.path.exists(filepath):
                self.skiplist_sender = FileList(filepath)
        if self.skiplist_sender is not None:
            skiplist = self.skiplist_sender.get_list()
            if suspect.from_domain in skiplist or suspect.from_address in skiplist:
                return True
        return False
    
    
    def _skip_sender_host(self, suspect):
        if self.skiplist_hostnames is None:
            filepath = self.config.get(self.section, 'skiplist_hostnames')
            if filepath and os.path.exists(filepath):
                self.skiplist_hostnames = FileList(filepath)
        client_host = suspect.get_value('client_name')
        if self.skiplist_hostnames is not None and client_host:
            skiplist = self.skiplist_hostnames.get_list()
            client_domain = self.tldmagic.get_domain(client_host)
            if client_host in skiplist or client_domain in skiplist:
                return True
        return False
    
    
    def linefilter_to_cidr(self, line):
        if is_cidrv4(line):
            line = ipaddress.ip_network(line)
        elif is_ip(line):
            line = ipaddress.ip_network('%s/32' % line)
        else:
            line = None
        return line
        
    
    def _skip_sender_ip(self, suspect):
        if self.skiplist_hostips is None:
            filepath = self.config.get(self.section, 'skiplist_hostips')
            if filepath and os.path.exists(filepath):
                self.skiplist_hostips = FileList(filepath, additional_filters=[self.linefilter_to_cidr])
        client_ip = ipaddress.ip_address(suspect.get_value('client_ip'))
        if self.skiplist_hostips is not None:
            skiplist = self.skiplist_hostips.get_list()
            for network in skiplist:
                if client_ip in network:
                    return True
        return False
        
    
    
    def examine(self,suspect):
        if not REDIS_ENABLED:
            return DUNNO
            
        queue_id = suspect.get_value('queue_id') or 'NOQUEUE'
        
        if self._skip_tags(suspect):
            return DUNNO
        
        if self._skip_sender(suspect):
            self.logger.debug('%s greylist: skipping check for sender %s' % (queue_id, suspect.from_address))
            return DUNNO
        
        if self._skip_sender_host(suspect):
            self.logger.debug('%s greylist: skipping check for sender host %s' % (queue_id, suspect.get_value('client_name')))
            return DUNNO
        
        if self._skip_sender_ip(suspect):
            self.logger.debug('%s greylist: skipping check for sender host %s' % (queue_id, suspect.get_value('client_ip')))
            return DUNNO
            
        self._init_backend()
        
        use_from_domain = self.config.getboolean(self.config, 'use_from_domain')
        if use_from_domain:
            sender = suspect.from_domain
        else:
            sender = suspect.from_address
        
        use_sender_host = self.config.getboolean(self.config, 'use_sender_host')
        if use_sender_host:
            sender_host = suspect.get_value('client_name')
            if sender_host is not None:
                sender_host = self.tldmagic.get_domain(sender_host)
            else:
                sender_host = suspect.get_value('client_address')
                sender_host = self._normalise_cidr(sender_host)
        else:
            sender_host = ''

        use_to_domain = self.config.getboolean(self.config, 'use_to_domain')
        if use_to_domain:
            recipient = suspect.to_domain
        else:
            recipient = suspect.to_address
        
        greylist_key = '%s\n%s\n%s' % (sender, sender_host, recipient)

        message = apply_template(self.config.get(self.section, 'messagetemplate'), suspect)
        greylist_ok = self.backend.check(greylist_key)
        if greylist_ok is None:
            self.backend.set(greylist_key)
            return DEFER, message
        elif not greylist_ok:
            return DEFER, message
        
        self.backend.update(greylist_ok)
        return DUNNO
    
    
    def lint(self):
        ok = self.check_config()
        
        if not REDIS_ENABLED:
            print('ERROR: redis not available - this plugin will do nothing')
            ok = False
            return ok
        
        for item in ['skiplist_sender', 'skiplist_hostnames', 'skiplist_hostips']:
            filepath = self.config.get(self.section, item)
            if filepath and not os.path.exists(filepath):
                print('ERROR: %s file %s not found' % (item, filepath))
                ok = False
        
        return ok
        



class RedisBackend(object):
    def __init__(self, redisconn=None):
        self.redis = redisconn or redis.StrictRedis()
        self.ttl = 7 * 24 * 3600
        self.minwait = 300
        self.logger = logging.getLogger("fuglu.greylist.RedisBackend")
    
    def set(self, key):
        value = int(time.time())
        self.redis.set(key, value, ex=self.ttl, nx=True)
    
    def update(self, key):
        self.redis.expire(key, self.ttl)
    
    def check(self, key):
        ts = None
        try:
            ts = self.redis.get(key)
            if ts:
                ts = int(ts)
                now = int(time.time())
                if ts + self.minwait < now:
                    self.logger.debug('known key %s first seen sufficiently long ago' % key)
                    return True
                else:
                    self.logger.debug('known key %s first seen too short ago' % key)
                    return False
            else:
                self.logger.debug('unknown key %s' % key)
                return None
        except (ValueError, TypeError):
            self.logger.error('known key %s has garbage content %s' % (key, ts))
            self.redis.delete(key)
            return None
        except (socket.timeout, redis.exceptions.TimeoutError):
            self.logger.info("Socket timeout in check while getting key %s" % key)
            return None
