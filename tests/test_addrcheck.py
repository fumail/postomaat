# -*- coding: utf-8 -*-
import unittest
from postomaat.addrcheck import Addrcheck


class TestDefault(unittest.TestCase):
    """Tests for the default Address validation"""

    def test_default_pass(self):
        """Valid mail should pass"""
        defaultchecker = Addrcheck()
        self.assertTrue(defaultchecker.valid("bala@blabla.bla"))

    def test_default_fail_2addchars(self):
        """Having 2 '@' chars should fail"""
        defaultchecker = Addrcheck()
        self.assertFalse(defaultchecker.valid("bala@bla@bla.bla"))

    def test_default_nonascii(self):
        """Non-ascii char should fail"""
        defaultchecker = Addrcheck()
        self.assertFalse(defaultchecker.valid("bäla@blabla.bla"))

    def test_apostrophe(self):
        """Apostrophe is ascii and therefore allowed"""
        defaultchecker = Addrcheck()
        self.assertTrue(defaultchecker.valid("aaaaaa.a'aaaaaa@aa.aaaaaaa.aaa"))

    def test_control_char(self):
        """Address with control character"""
        defaultchecker = Addrcheck()
        self.assertTrue(defaultchecker.valid("aaa.aaaaaaa@aaaaaaa-aaaaaa.aa"))

