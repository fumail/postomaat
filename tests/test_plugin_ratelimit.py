import unittest
import tempfile
import time
import random
import logging

from configparser import RawConfigParser

import yaml

from postomaat.plugins.ratelimit.main import RateLimitPlugin, CounterInt, Limiter
from postomaat.shared import Suspect, DUNNO, REJECT, DEFER, apply_template


random.seed()

# ------ #
# Mixins #
# ------ #

class PrintTitleMixin:
    """Mixin for setup - print title, setup logger config"""
    backend = ""

    def _print_title(self, title: str):
        title_line = f"- ({self.backend}) {title} -"
        title_dash = len(title_line)*'-'
        print(f"\n{title_dash}\n{title_line}\n{title_dash}\n")

        logging.basicConfig(level=logging.DEBUG)


class FixedTestMixin:
    """Mixin for fixed-strategy tests with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _get_fixed_config(self, suspectattr: bool = True):
        """
        Setup config for fixed rate

        'suspectattr' defines if suspect attribute should be used or
        directly the protocol dictionary to extract the sender
        """
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        # one event per second
        if suspectattr:
            limiterfile.write(f"""
{name}:
    rate: 1/1
    strategy: fixed
    action: REJECT  
    message: {rejectmessage}
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        else:
            limiterfile.write(f"""
{name}:
    rate: 1/1
    strategy: fixed
    action: REJECT  
    message: {rejectmessage}
    count:
        sender:
            type: Protocoldict
            regex: ^.*@domain.invalid$
""".encode())

        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        return limiterfile, rejectmessage, sender, config

    def _test_fixed(self, suspectattr: bool = True):
        """
        Test fixed strategy.

        'suspectattr' defines if suspect attribute should be used or
        directly the protocol dictionary to extract the sender
        """
        limiterfile, rejectmessage, sender, config = self._get_fixed_config(suspectattr=suspectattr)

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with the first run")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Immediate second run should be blocked")

        sleeptime = 1.05
        print(f"sleep for {sleeptime}s which should release block")
        time.sleep(sleeptime)

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        self.assertEqual(DUNNO, result, "Block should be released")

    def _get_fixed_match_config(self, domaincount: bool = False):
        """Test fixed rate"""
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        if domaincount:
            rejectmessage = "Too many messages from this domain"
        else:
            rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        # one event per second
        if domaincount:
            limiterfile.write(f"""
{name}:
    rate: 1/1
    strategy: fixed
    action: REJECT
    message: {rejectmessage}
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
            key: domain.invalid
""".encode())
        else:
            limiterfile.write(f"""
{name}:
    rate: 1/1
    strategy: fixed
    action: REJECT
    message: {rejectmessage}
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        return limiterfile, rejectmessage, sender, config

    def _test_fixed_match(self):
        """Test fixed rate with regex match defining if limiter is applied"""
        limiterfile, rejectmessage, sender, config = self._get_fixed_match_config()

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with the first run")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Immediate second run should be blocked")

        sender_unlimited = "sender@free.domain.invalid"
        suspect = Suspect(
            values={
                'sender': sender_unlimited,
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with the first run for unlimited")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        self.assertEqual(DUNNO, result, "No problem with the second run for unlimited")

    def _test_fixed_match_key(self):
        """Test fixed rate with regex match and counter defined for domain"""
        limiterfile, rejectmessage, sender, config = self._get_fixed_match_config(domaincount=True)

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': 'sender1@domain.invalid',
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with the first run")

        suspect = Suspect(
            values={
                'sender': 'sender2@domain.invalid',
                'recipient': "recipient@domain.invalid",
            }
        )
        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Immediate second run should be blocked")

        # now another sender, same domain
        suspect = Suspect(
            values={
                'sender': 'sender3@domain.invalid',
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        self.assertEqual((REJECT, expectedreject), result, "Domaincount -> this sender should be blocked as well")


class FixedSlidingTestMixin:
    """Mixin to run fixed strategy with a setup for a sliding strategy with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _get_fixed_slidingsetup_config(self):
        """Test fixed rate where sliding setup would pass"""
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        # one event per second
        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        limiterfile.write(f"""
{name}:
    rate: 2/1
    strategy: fixed
    action: REJECT
    message: {rejectmessage}
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)
        return limiterfile, rejectmessage, sender, config

    def _test_fixed_slidingsetup(self):
        """Run test for sliding setup with a fixed strategy"""
        limiterfile, rejectmessage, sender, config = self._get_fixed_slidingsetup_config()

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
            }
        )
        expectedreject = apply_template(rejectmessage, suspect=suspect)

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with 1st run")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        # still ok until here, there were 2 messages in 0.6
        self.assertEqual(DUNNO, result, "No problem with 2nd run")

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        self.assertEqual((REJECT, expectedreject), result, f"Reject expected")

        sleeptime = 2.1
        print(f"Sleep for {sleeptime}s")
        time.sleep(sleeptime)
        result = candidate.examine(suspect=suspect)
        print(f"Result 4: {result}")
        self.assertEqual(DUNNO, result, f"No problem expected after waiting for {sleeptime}s")


class SlidingTestMixin:
    """Mixin to run sliding-log strategy with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _test_sliding(self):
        """Config and run test for sliding-log strategy"""
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        # one event per second

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        limiterfile.write(f"""
{name}:
    rate: 2/1
    strategy: sliding-log
    action: REJECT
    message: {rejectmessage}
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with first message")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        self.assertEqual(DUNNO, result, f"No problem for 2nd message")

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Problem if 3 msgs right one after the other")

        sleeptime = 2.1
        print(f"Sleep for {sleeptime}s")
        time.sleep(sleeptime)

        result = candidate.examine(suspect=suspect)
        print(f"Result 5: {result}")
        self.assertEqual(DUNNO, result, f"No problem after waiting for {sleeptime}s")


class MessageSizeMixin:
    """Mixin to run data rate limit tests with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _get_config(self, strategy: str="fixed"):

        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                              prefix='postomaat-unittest',
                                              dir='/tmp'
                                              )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        # one event per second

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        limiterfile.write(f"""
{name}:
    rate: 40/1
    strategy: {strategy}
    action: REJECT
    message: {rejectmessage}
    sum: data
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        return limiterfile, rejectmessage, sender, config

    def _test_rate_singlemessages(self, strategy: str="fixed"):
        limiterfile, rejectmessage, sender, config = self._get_config(strategy=strategy)
        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
                'size': '15'
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with 1st message (15 bytes < 40 bytes)")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        self.assertEqual(DUNNO, result, "No problem with 2nd message (30 bytes < 40 bytes)")

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Problem with 3rd message (45 bytes < 40 bytes)")

        time.sleep(2.05)
        result = candidate.examine(suspect=suspect)
        print(f"Result 4: {result}")
        self.assertEqual(DUNNO, result, "No problem with 4th message after sleeping 1.5 s")

    def _test_rate_multiplemessages(self, strategy: str="fixed", withlint: bool=False):
        limiterfile, rejectmessage, sender, config = self._get_config(strategy=strategy)
        candidate = self.RLCLASS(config=config)
        if withlint:
            if not candidate.lint():
                raise ValueError("Lint failed!")

        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
                'size': '15',
                'recipient_count': '2'
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with message 1&2 (2*15 bytes < 40 bytes)")

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Problem with 3&4 message (4*15 bytes > 40 bytes)")

        time.sleep(2.25)
        result = candidate.examine(suspect=suspect)
        print(f"Result 4: {result}")
        self.assertEqual(DUNNO, result, "No problem with 5&6 message after sleeping 1.5 s")


class RecipientCountMixin:
    """Mixin to test recipient count 'sum' method with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _get_reccount_config(self, strategy: str="fixed"):

        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        # one event per second

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        limiterfile.write(f"""
{name}:
    rate: 40/1
    strategy: {strategy}
    action: REJECT
    message: {rejectmessage}
    sum: recipients
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        return limiterfile, rejectmessage, sender, config

    def _test_rate_recipientcount(self, strategy: str="fixed"):
        """Run test where recipient_count is used to sum up"""
        limiterfile, rejectmessage, sender, config = self._get_reccount_config(strategy=strategy)
        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
                'recipient_count': '15'
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with 1st message (15 messages < 40)")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        self.assertEqual(DUNNO, result, "No problem with 2nd message (30 messages < 40)")

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Problem with 3rd message (45 messages > 40)")

        time.sleep(1.05)
        result = candidate.examine(suspect=suspect)
        print(f"Result 4: {result}")
        self.assertEqual(DUNNO, result, "No problem with 4th message after sleeping 1.5 s")


class SlidingWindowTestMixin:
    """Mixin to run sliding-window strategy with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _test_slidingbuckets(self):
        """Config & test for sliding-window strategy"""
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        # one event per second

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        limiterfile.write(f"""
{name}:
    rate: 3/60
    strategy: sliding-window
    action: REJECT
    message: {rejectmessage}
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
""".encode())
        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
            }
        )

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with first message")

        sleeptime = 0.05
        print(f"Sleep for {sleeptime}s")
        time.sleep(sleeptime)

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        self.assertEqual(DUNNO, result, f"No problem for message after {sleeptime}s")

        sleeptime = 0.05
        print(f"Sleep for {sleeptime}s")
        time.sleep(sleeptime)

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        self.assertEqual(DUNNO, result, f"No problem for message after {sleeptime}s")

        result = candidate.examine(suspect=suspect)
        print(f"Result 4: {result}")
        expectedreject = apply_template(rejectmessage, suspect=suspect)
        self.assertEqual((REJECT, expectedreject), result, "Problem if 2 msgs right one after the other")


class CountSuspectTagMixin:
    """Mixin for Suspect Tag test with different backends"""
    backend = ""
    backendconfig = ""
    RLCLASS = RateLimitPlugin

    def assertEqual(self, *args, **kwargs):
        raise NotImplementedError()

    def _get_fixed_config_tag(self):
        """
        Setup config for fixed rate, counting suspect tag
        """
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        randomint = str(random.randint(1, 999))
        name = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={name}")
        # one event per second
        limiterfile.write(f"""
{name}:
    rate: 1/1
    strategy: fixed
    action: REJECT  
    message: {rejectmessage}
    count:
        custom:
            type: SuspectTag
""".encode())

        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        return limiterfile, rejectmessage, sender, config

    def _test_fixed_tag(self, tagexists: bool = True):
        """
        Test fixed strategy with suspect tag.

        'tagexists' defines if suspect tag exists
        """
        limiterfile, rejectmessage, sender, config = self._get_fixed_config_tag()

        candidate = self.RLCLASS(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "recipient@domain.invalid",
            }
        )
        if tagexists:
            suspect.set_tag("custom", "countthis")

        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual(DUNNO, result, "No problem with the first run")

        result = candidate.examine(suspect=suspect)
        print(f"Result 2: {result}")
        if tagexists:
            expectedreject = apply_template(rejectmessage, suspect=suspect)
            self.assertEqual((REJECT, expectedreject), result, "Immediate second run should be blocked")
        else:
            self.assertEqual(DUNNO, result, "No problem if tag is not found")

        sleeptime = 1.05
        print(f"sleep for {sleeptime}s which should release block")
        time.sleep(sleeptime)

        result = candidate.examine(suspect=suspect)
        print(f"Result 3: {result}")
        self.assertEqual(DUNNO, result, "Block should be released")


# ---- #
# Test #
# ---- #


class TestMemoryBackend(unittest.TestCase,
                        FixedTestMixin,
                        FixedSlidingTestMixin,
                        SlidingTestMixin,
                        MessageSizeMixin,
                        RecipientCountMixin,
                        CountSuspectTagMixin,
                        PrintTitleMixin,
                        ):
    """Tests using memory backend"""
    backend = "memory"
    backendconfig = ""

    def test_fixed(self):
        """Test fixed rate with memory backend"""
        self._print_title("Test fixed")
        self._test_fixed()

    def test_fixed_pdict(self):
        """Test fixed rate (protocol dict) with memory backend"""
        self._print_title("Test fixed (Protocol dict)")
        self._test_fixed(suspectattr=False)

    def test_fixed_match(self):
        """Test fixed rate with memory backend"""
        self._print_title("Test fixed with match")
        self._test_fixed_match()

    def test_fixed_domain(self):
        """Test fixed rate for whole domain"""
        self._print_title("Test fixed for domain")
        self._test_fixed_match_key()

    def test_fixed_slidingsetup(self):
        """Test fixed rate with memory backend"""
        self._print_title("Test fixed slidingsetup")
        self._test_fixed_slidingsetup()

    def test_sliding(self):
        """Test sliding-window rate with memory backend"""
        self._print_title("Test sliding")
        self._test_sliding()

    def test_datarate(self):
        """Test sliding-window rate with memory backend"""
        self._print_title("Test data-rate singlemessages")
        self._test_rate_singlemessages()

    def test_datarate_slog(self):
        """Test sliding-log rate with memory backend"""
        self._print_title("Test data-rate singlemessages")
        self._test_rate_singlemessages(strategy="sliding-log")

    def test_multim_datarate(self):
        """Test sliding-window data rate (multiple messages) with memory backend"""
        self._print_title("Test data-rate multiplemessages")
        self._test_rate_multiplemessages()

    def test_multim_datarate_slog(self):
        """Test sliding-log data rate (multiple messages) with memory backend"""
        self._print_title("Test data-rate multiplemessages")
        self._test_rate_multiplemessages(strategy="sliding-log")

    def test_backend_missing(self):
        """Sliding log not available for memory backend"""
        self._print_title("Test lint error (backend missing)")
        with self.assertRaises(ValueError):
            self._test_rate_multiplemessages(strategy="sliding-window", withlint=True)

    def test_rate_recipientcount(self):
        """Test recipient count summation"""
        self._print_title("Test recipient_count summation")
        self._test_rate_recipientcount()

    def test_rate_suspecttag(self):
        """Test suspect tag limit"""
        self._print_title("Test count on suspect tag")
        self._test_fixed_tag(tagexists=True)

    def test_rate_suspecttag_none(self):
        """Test suspect tag limit nonexisting"""
        self._print_title("Test non count on nonexisting suspect tag")
        self._test_fixed_tag(tagexists=False)


class TestRedisBackend(unittest.TestCase,
                       FixedTestMixin,
                       FixedSlidingTestMixin,
                       SlidingTestMixin,
                       SlidingWindowTestMixin,
                       MessageSizeMixin,
                       RecipientCountMixin,
                       CountSuspectTagMixin,
                       PrintTitleMixin,
                       ):
    """Tests using Redis backend"""
    backend = "redis"
    backendconfig = "redis://redis:6379/0"

    def test_fixed(self):
        """Test fixed rate with redis backend"""
        self._print_title("Test fixed")
        self._test_fixed()

    def test_fixed_slidingsetup(self):
        """Test fixed rate with redis backend"""
        self._print_title("Test fixed slidingsetup")
        self._test_fixed_slidingsetup()

    def test_sliding(self):
        """Test sliding-window rate with redis backend"""
        self._print_title("Test sliding")
        self._test_sliding()

    def test_slidingwindow(self):
        """Test sliding-window rate with redis backend"""
        self._print_title("Test sliding")
        self._test_slidingbuckets()

    def test_datarate(self):
        """Test sliding-window data rate with redis backend"""
        self._print_title("Test data-rate singlemessages")
        self._test_rate_singlemessages()

    def test_datarate_swindow(self):
        """Test sliding-window data rate with redis backend"""
        self._print_title("Test data-rate singlemessages (sliding)")
        self._test_rate_singlemessages(strategy="sliding-window")

    def test_multim_datarate(self):
        """Test sliding-window data rate (multiple messages) with redis backend"""
        self._print_title("Test data-rate multiplemessages")
        self._test_rate_multiplemessages()

    def test_multim_datarate_swindow(self):
        """Test sliding-window data rate (multiple messages) with redis backend"""
        self._print_title("Test data-rate multiplemessages (sliding)")
        self._test_rate_multiplemessages(strategy="sliding-window")

    def test_rate_recipientcount(self):
        """Test recipient count summation"""
        self._print_title("Test recipient_count summation")
        self._test_rate_recipientcount()

    def test_rate_suspecttag(self):
        """Test suspect tag limit"""
        self._print_title("Test count on suspect tag")
        self._test_fixed_tag(tagexists=True)

    def test_rate_suspecttag_none(self):
        """Test suspect tag limit nonexisting"""
        self._print_title("Test non count on nonexisting suspect tag")
        self._test_fixed_tag(tagexists=False)


class TestRule(unittest.TestCase):
    def test_ratestring(self):
        """Test string for rate"""
        number = 2
        frame = 4.4
        parsed_number, parsed_frame = Limiter._parse_rate(f"{number}/{frame}")
        self.assertEqual(parsed_number, number)
        self.assertEqual(parsed_frame, frame)

    def test_ratedictstring(self):
        """Test dict for rate"""
        number = 2
        frame = 4.4
        parsed_number, parsed_frame = Limiter._parse_rate({'number': f"{number}",
                                                         'frame': f"{frame}"})
        self.assertEqual(parsed_number, number)
        self.assertEqual(parsed_frame, frame)

    def test_regex_string(self):
        """Test regex strings"""
        reg = CounterInt._parse_regex(r"^.*\@domain.invalid$")
        self.assertIs(1, len(reg))

    def test_regex_stringlist(self):
        """Test list of regex strings"""
        reg = CounterInt._parse_regex([r"^.*\@domain.invalid$", r"^.*\@domain.invalid$"])
        self.assertIs(2, len(reg))

    def test_regex_string_error(self):
        """Test if regex with error raises an exception"""
        with self.assertRaises(Exception):
            _ = CounterInt._parse_regex(r"(")

    def test_noregex(self):
        """Without regex defined for the limiter every call should be a hit"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        from_address:
            type: Suspect
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])
        values = {'sender': 'sender@domain.invalid',
                  'recipient': 'recipient@domain.invalid'}

        # create suspect which should match limiter conditions
        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # any suspect will match limiter condition
        values = {'sender': 'sender1@domain.invalid',
                  'recipient': 'recipient@domain.invalid'}
        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

    def test_loadsingleregex(self):
        """Check single regex match for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        from_address:
            type: Suspect
            regex: ^.*@domain.invalid$
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])
        values = {'sender': 'sender@domain.invalid',
                  'recipient': 'recipient@domain.invalid'}

        # create suspect which should match limiter conditions
        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        values = {'sender': 'sender@another.domain.invalid',
                  'recipient': 'recipient@domain.invalid'}
        suspect = Suspect(values=values)
        self.assertFalse(r.examine(suspect=suspect))

    def test_loadmultiregex(self):
        """Check any regex match for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        from_address:
            type: Suspect
            regex:
                - ^.*@domain.invalid$
                - ^.*@another.domain.invalid$
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])

        # create suspect which should match first limiter regex condition
        values = {'sender': 'sender@domain.invalid',
                  'recipient': 'recipient@domain.invalid'}

        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should match second limiter regex condition
        values = {'sender': 'sender@another.domain.invalid',
                  'recipient': 'recipient@domain.invalid'}
        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        values = {'sender': 'sender@disabled.domain.invalid',
                  'recipient': 'recipient@domain.invalid'}
        suspect = Suspect(values=values)
        self.assertFalse(r.examine(suspect=suspect))

    def test_net_string(self):
        """Test network strings"""
        nets = CounterInt._parse_ipmatch(r"192.168.1.1/16")
        self.assertIs(1, len(nets))

    def test_net_stringlist(self):
        """Test list of regex strings"""
        nets = CounterInt._parse_ipmatch([r"192.168.1.1/24", r"192.168.2.1/24"])
        self.assertIs(2, len(nets))

    def test_net_string_error(self):
        """Test if regex with error raises an exception"""
        with self.assertRaises(Exception):
            _ = CounterInt._parse_ipmatch(r"192.168")

    def test_loadsingleipmatch(self):
        """Check single ipmatch for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        client_address:
            type: ProtocolDict
            ipmatch: 192.168.1.1/24
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])
        values = {'sender': 'sender@domain.invalid',
                  'recipient': 'recipient@domain.invalid',
                  'client_address': '192.168.1.1'}

        # create suspect which should match limiter conditions
        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        values = {'sender': 'sender@another.domain.invalid',
                  'recipient': 'recipient@domain.invalid',
                  'client_address': '192.168.2.1'}
        suspect = Suspect(values=values)
        self.assertFalse(r.examine(suspect=suspect))

    def test_loadmultiipmatch(self):
        """Check any regex match for limiter"""
        setupdict = yaml.safe_load("""
dummyfromrule2:
    rate: 5/10
    action: DUNNO    
    count:
        client_address:
            type: ProtocolDict
            ipmatch:
                - 192.168.1.0/24
                - 192.168.2.0/24
        """)
        r = Limiter(name="test", setupdict=setupdict["dummyfromrule2"])

        # create suspect which should match first limiter regex condition
        values = {'sender': 'sender@domain.invalid',
                  'recipient': 'recipient@domain.invalid',
                  'client_address': '192.168.1.1'}

        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should match second limiter regex condition
        values = {'sender': 'sender@another.domain.invalid',
                  'recipient': 'recipient@domain.invalid',
                  'client_address': '192.168.2.1'}
        suspect = Suspect(values=values)
        self.assertTrue(r.examine(suspect=suspect))

        # create suspect which should NOT match limiter conditions
        values = {'sender': 'sender@disabled.domain.invalid',
                  'recipient': 'recipient@domain.invalid',
                  'client_address': '192.168.3.1'}
        suspect = Suspect(values=values)
        self.assertFalse(r.examine(suspect=suspect))

class TestYaml2Dict(unittest.TestCase,
                        PrintTitleMixin,
                        ):
    """Yaml - Dict conversion test"""
    backend = "memory"
    backendconfig = ""

    def test_yaml2dict(self):
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        randomint = str(random.randint(1, 9999))
        rulename = f"{self.__class__.__name__}_{self.backend}_{randomint}"
        print(f"Setup config line with rule name={rulename}")

        rulesdict = {
            rulename: {
                'rate': {
                    'number': 1,
                    'frame': 1
                },
                'count': {
                    'from_address': {
                        'type': 'Suspect',
                        'regex':  r'^.*@domain.invalid$'
                    }
                },
                'fields': ['from_address'],
                'action': 'REJECT',
                'message': "Too many messages from ${from_address}"
            }
        }

        limiterfile.write(yaml.dump(rulesdict).encode())
        limiterfile.seek(0)

        extracteddict = RateLimitPlugin.yamlfile2dict(limiterfile.name)
        self.assertEqual(rulesdict, extracteddict)


class TestWhitelisting(unittest.TestCase, PrintTitleMixin):
    """Test the 'always-hit' backend without db which can be used for whitelistings and testing"""
    backend = "memory"
    backendconfig = ""

    def _get_config(self, reverse_order: bool = False):
        """Get config, 'reverse_order=True' will disable whitelisting"""
        limiterfile = tempfile.NamedTemporaryFile(suffix='RLimit',
                                                  prefix='postomaat-unittest',
                                                  dir='/tmp'
                                                  )

        rejectmessage = "Too many messages from ${from_address}"
        sender = "sender@domain.invalid"

        randomint = str(random.randint(1, 999))
        name_exc = f"{self.__class__.__name__}_recipient_whitelist_{randomint}"
        randomint = str(random.randint(1, 999))
        name_gen = f"{self.__class__.__name__}_general_limit_{randomint}"
        print(f"Setup config line with rule name={name_exc},{name_gen}")

        # one event per second
        limiterfile.write(f"""
{name_exc}:
    priority: {'2' if reverse_order else '1'}
    strategy: always-hit
    rate: 1/1
    message: ${{from_address}} to ${{to_address}} has an exception
    action: DUNNO
    count:
        to_address:
            type: Suspect 
            regex: ^allowed\@domain\.invalid$
        from_address:
            type: Suspect
            regex: ^.*\@domain\.invalid$
{name_gen}:
    priority: {'1' if reverse_order else '2'}
    strategy: always-hit
    rate: 1/1
    message: ${{from_address}} is sending too fast
    action: DEFER
    count:
        sender:
            type: protocoldict
            regex: ^.*\@domain\.invalid$
""".encode())

        limiterfile.seek(0)

        config = RawConfigParser()
        config.add_section('RateLimitPlugin')
        config.set('RateLimitPlugin', 'limiterfile', limiterfile.name)
        config.set('RateLimitPlugin', 'backendtype', self.backend)
        config.set('RateLimitPlugin', 'backendconfig', self.backendconfig)

        return limiterfile, rejectmessage, sender, config

    def test_whitelist(self):
        """Test if whitelist applies first"""
        self._print_title("Test whitelist")
        limiterfile, rejectmessage, sender, config = self._get_config()
        candidate = RateLimitPlugin(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "allowed@domain.invalid",
            }
        )
        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual((DUNNO, "sender@domain.invalid to allowed@domain.invalid has an exception"), result, "Whitelist should apply here")

    def test_whitelist_disabled_by_order(self):
        """Test if whitelist is disabled by order key applying general rule first"""
        self._print_title("Test whitelist")
        limiterfile, rejectmessage, sender, config = self._get_config(reverse_order=True)
        candidate = RateLimitPlugin(config=config)
        suspect = Suspect(
            values={
                'sender': sender,
                'recipient': "allowed@domain.invalid",
            }
        )
        result = candidate.examine(suspect=suspect)
        print(f"Result 1: {result}")
        self.assertEqual((DEFER, "sender@domain.invalid is sending too fast"), result, "Whitelist should not apply here")
