postomaat
=========

A policy daemon for postfix written in python

This is a  by-product of the [Fuglu](https://gitlab.com/fumail/fuglu) mailfilter.
While fuglu focuses on scanning a full message (pre- or after-queue), postomaat only uses the message
fields available in the  [Postfix policy access delegation protocol](http://www.postfix.org/SMTPD_POLICY_README.html)
It can therefore make decisions much faster than fuglu, but only based on envelope data (sender adress, recipient adress, client ip etc).
Postomaat can not make decisions based on message headers or body.

Warning: Postomaat currently doesn't receive the same testing as fuglu before commiting to gitlab.
The master branch therefore might or might not work out of the box.  

END OF LIFE
===========

Postomaat has reached end of life and will no longer be officially maintained. All functionality previously provided by Postomaat policy daemon has been implemented in Fuglu (running as milter or prequeue). We recommend all users to replace their postomaat setups by fuglu milter.
